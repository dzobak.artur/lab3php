
    <h2>Create Zapravka</h2>

    <form method="POST" action="<?php echo e(route('zapravkas.store')); ?>">
        <?php echo csrf_field(); ?>
        <div>
            <label for="address">Address:</label>
            <input type="text" name="address" id="address" required>
        </div>

        <div>
            <label for="fullname">Fullname:</label>
            <input type="text" name="fullname" id="fullname" required>
        </div>

        <div>
            <label for="fuel">Fuel:</label>
            <input type="text" name="fuel" id="fuel" required>
        </div>

        <div>
            <label for="fuelprice">Fuel Price:</label>
            <input type="text" name="fuelprice" id="fuelprice" required>
        </div>

        <div>
            <button type="submit">Create</button>
        </div>
    </form>

    <a href="<?php echo e(route('zapravkas.index')); ?>">Back to List</a>

<?php /**PATH C:\xampp\htdocs\lab3\laravel\resources\views/zapravkas/create.blade.php ENDPATH**/ ?>